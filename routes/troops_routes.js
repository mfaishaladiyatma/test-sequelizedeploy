const { TroopsController } = require("../controller")
const route = require("express").Router()
const { authentication, TroopsApproval } = require("../middleware")

route.get("/", TroopsController.getTroops)
route.get("/:TroopsId", TroopsController.getTroopsById)
route.post("/login", TroopsController.login)
route.post("/", TroopsController.createTroops)

route.use(authentication)

route.put("/:TroopsId", TroopsApproval, TroopsController.updateTroops)
route.delete("/:TroopsId", TroopsApproval, TroopsController.deleteTroops)

module.exports = route