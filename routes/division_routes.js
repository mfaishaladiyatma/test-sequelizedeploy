const { DivisionController } = require("../controller")
const route = require("express").Router()

route.get("/", DivisionController.getDivisions)
route.get("/:id", DivisionController.getDivisionById)
route.post("/", DivisionController.createDivisions)
route.put("/:id", DivisionController.updateDivisions)
route.delete("/:id", DivisionController.deleteDivisions)

module.exports = route