const TroopsRoute = require("./troops_routes")
const WeaponsRoute = require("./weapons_routes")
const DivisionRoute = require("./division_routes")
const TroopsDivisionRoute = require("./troopsDivision_routes")
const route = require("express").Router()

route.use("/troops", TroopsRoute)
route.use("/weapons", WeaponsRoute)
route.use("/division", DivisionRoute)
route.use("/troopsDivision", TroopsDivisionRoute)


module.exports = route