const { WeaponsController } = require("../controller")
const route = require("express").Router()



route.get("/", WeaponsController.getWeapons)
route.get("/:WeaponsId", WeaponsController.getWeaponsById)
route.post("/", WeaponsController.createWeapons)
route.put("/:WeaponsId", WeaponsController.updateWeapons)
route.delete("/:WeaponsId", WeaponsController.deleteWeapons)

module.exports = route