const { TroopsDivisionController } = require("../controller")
const route = require("express").Router()
const { authentication } = require("../middleware")

route.get("/", TroopsDivisionController.getAllTroopsDivision)
route.get("/:id", TroopsDivisionController.getTroopsDivisionById)
route.post("/", TroopsDivisionController.createTroopsDivision)

route.use(authentication)

route.put("/:id", TroopsDivisionController.updateTroopsDivision)
route.delete("/:id", TroopsDivisionController.deleteTroopsDivision)

module.exports = route