'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('TroopsDivisions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      TroopsId: {
        type: Sequelize.STRING,
        references: {
          model: "Troops",
          key: "TroopsId"
        }
      },
      DivisionId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Divisions",
          key: "id",
          unique: true
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('TroopsDivisions');
  }
};