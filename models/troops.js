'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Troops extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Troops.hasMany(models.Weapons)
      Troops.belongsToMany(models.Divisions, {
        through: models.TroopsDivision, foreignKey: "TroopsId"
      })
    }
  }
  Troops.init({
    TroopsId: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    rank: DataTypes.STRING,
    age: DataTypes.INTEGER,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Troops',
  });
  return Troops;
};