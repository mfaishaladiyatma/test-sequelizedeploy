'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Weapons extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Weapons.belongsTo(models.Troops, {
        foreignKey: "TroopTroopsId"
      })
    }
  }
  Weapons.init({
    WeaponsId: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    TroopTroopsId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Weapons',
  });
  return Weapons;
};