'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TroopsDivision extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Troops.belongsToMany(models.Divisions, { through: models.TroopsDivision, foreignKey: "TroopsId" })
      models.Divisions.belongsToMany(models.Troops, { through: models.TroopsDivision, foreignKey: "DivisionId" })
    }
  }
  TroopsDivision.init({
    TroopsId: DataTypes.STRING,
    DivisionId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'TroopsDivision',
  });
  return TroopsDivision;
};