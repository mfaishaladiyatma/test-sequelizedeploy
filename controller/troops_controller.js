const { Troops, Weapons } = require("../models")
const uuid = require("uuid")
const { Token, encryption } = require("../utils")





class TroopsController {
    static async login(req, res) {
        const { name, password } = req.body
        const troop = await Troops.findOne({
            where: {
                name
            }
        })

        const isValid = troop ? encryption.PasswordValid(password, troop.dataValues.password) : 0
        console.log(troop)

        if (!troop || !isValid) {
            res.status(404).json({
                status: 404,
                msg: "Anda Siapa?!!"
            })
            return
        }

        const access_token = Token.createToken(troop.dataValues)
        res.status(200).json({ access_token })
    }
    
    static async getTroops(req, res) {
        try {
            const options = {
                include: [
                    {
                        model: Weapons,
                        attributes: ["name", "type"]
                    }
                ]
            }
            const data = await Troops.findAll(options)
            res.status(200).json({ data })
        } catch (error){
            console.log(error)
        }
    }

    static async getTroopsById(req, res) {
        const { TroopsId } = req.params
        const options = {
            where: {
                TroopsId
            },
            include: [
                {
                    model: Weapons
                }
            ]
        }
        const data = await Troops.findOne(options)

        res.status(200).json({ data: data })
    }

    static async updateTroops(req, res) {
        const { name, rank, age } =req.body

        const payload = {
            name, rank, age
        }
        const { TroopsId } = req.params
        const options = {
            where: {
                TroopsId
            },
            returning: true
        }
        const doneUpdate = await Troops.update(payload, options)

        res.status(200).json({ data: doneUpdate })
    }

    static async createTroops(req, res) {
        const TroopsId = uuid.v4()
        let { name, rank, age, password } = req.body
        password = encryption.encryptPassword(password)
        const payload = {
            name, rank, age, TroopsId, password
        }
        console.log(password,"<<< password di Create")

        const createdTroops = await Troops.create(payload)
        console.log(createdTroops)
        res.status(200).json({ data: createdTroops })
    }

    static async deleteTroops(req, res) {
        const { TroopsId } = req.params
        const deleteTroops = await Troops.destroy({
            where: {
                TroopsId
            },
            returning: true
        })

        res.status(200).json({ data: deleteTroops})
    }
}

module.exports = TroopsController