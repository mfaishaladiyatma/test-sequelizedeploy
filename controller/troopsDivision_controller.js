const { TroopsDivision, Troops, Divisions } = require("../models")

class TroopsDivisionController {
    static async getAllTroopsDivision(req, res) {
        try {
            const data = await TroopsDivision.findAll()

            res.status(200).json({ data })
        } catch (error) {
            console.log(error)
        }
    }

    static async getTroopsDivisionById(req, res) {
        const { id } = req.params
        const options = {
            where: {
                id
            }
        }
        const data = await TroopsDivision.findOne(options)
        res.status(200).json({ data })
    }

    static async createTroopsDivision(req, res) {
        try {
            const { TroopsId, DivisionId } = req.body
            const payload = {
                TroopsId, DivisionId
            }
            const newTroopsDivision = await TroopsDivision.create(payload)
            res.status(200).json( { data: newTroopsDivision })
        } catch (error) {
            console.log(error)
        }
    }

    static async updateTroopsDivision(req, res) {
        const { TroopsId, DivisionId } = req.body

        const payload = {
            TroopsId, DivisionId
        }
        const { id } = req.params
        const options = {
            where: {
                id
            },
            returning: true
        }
        const doneUpdated = await TroopsDivision.update(payload, options)

        res.status(200).json({ data: doneUpdated })
    }

    static async deleteTroopsDivision(req, res) {
        const { id } = req.params
        const deleted = await TroopsDivision.destroy({ 
            where: {
                id
            },
            returning: true
        })

        res.status(200).json({ data: deleted })
    }
}

module.exports = TroopsDivisionController