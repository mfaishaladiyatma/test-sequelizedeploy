const { Weapons, Troops } = require("../models")
const uuid = require("uuid")

class WeaponsController {
    static async getWeapons(req, res) {
        try {
            const options = {
                include: [ 
                    {
                        model: Troops,
                        attributes: ["name"]
                    }
                ]
            }
            const data = await Weapons.findAll(options)
            res.status(200).json({ data: data })
        } catch (error){
            console.log(error)
        }
    }

    static async getWeaponsById(req, res){
        const { WeaponsId } = req.params
        const options = {
            where: {
                WeaponsId
            },
        }
        const data = await Weapons.findOne(options)

        res.status(200).json({ data: data})
    }

    static async updateWeapons(req, res){
        const{ name, type, TroopTroopsId } = req.body

        const payload = {
            name, type, TroopTroopsId
        }
        const { WeaponsId } = req.params 
        const options = { 
            where: {
                WeaponsId
            },
            returning: true
        }
        const updateWeapons = await Weapons.update(payload, options)

        res.status(200).json({data: updateWeapons})
    }

    static async createWeapons(req, res) {
        const WeaponsId = uuid.v4()
        const { name, type, TroopTroopsId } = req.body
        const payload = {
            name, type, TroopTroopsId, WeaponsId
        }

        const createWeapons = await Weapons.create(payload)
        res.status(200).json({ data: createWeapons })
    }

    static async deleteWeapons(req, res){
        const { WeaponsId } = req.params
        const deleteWeapons = await Weapons.destroy({
            where: {
                WeaponsId
            },
            returning: true
        })

        res.status(200).json({ data: deleteWeapons })
    }
}

module.exports = WeaponsController