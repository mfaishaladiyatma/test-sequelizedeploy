const TroopsController = require("./troops_controller")
const WeaponsController = require("./weapons_controller")
const DivisionController = require("./divisions_controller")
const TroopsDivisionController = require("./troopsDivision_controller")


module.exports={ 
    TroopsController, WeaponsController, DivisionController, TroopsDivisionController
}