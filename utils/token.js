const jwToken = require("jsonwebtoken")

class Token {
    static createToken(payload) {
        return jwToken.sign(payload, "Silent")
    }

    static decodeToken(token) {
        console.log(token, "<<< Ini Token")

        try{
            const _return = jwToken.verify(token, "Silent")
            if (!_return) {
                return null
            }

            return _return
        } catch (error) {
            return null
        }
    }
}

module.exports = Token