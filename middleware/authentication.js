const { Token } = require("../utils")
const authentication = (req, res, next) => {
    const access_token = req.headers.token
    const payload = access_token ? Token.decodeToken(access_token) : 0
    console.log(payload, "<<<< payload di authentication")

    if (!payload || !access_token) {
        res.status(404).json({
            status: 404,
            msg: "Belum Login Bro !!"
        })
    } else {
        req.userLogin = payload
        console.log(req.userLogin, "Payload Check")
        next()

    }
}

module.exports = authentication