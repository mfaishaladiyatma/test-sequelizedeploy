const TroopsApproval = (req, res, next) => {
    try {
        const payload = req.userLogin
        const { TroopsId } = req.params

        console.log(payload, "<< Payload di Approval")

        const IdCheck = TroopsId ? payload.TroopsId === TroopsId : 0

        if (!IdCheck) {
            res.status(404).json({
                msg: "Anda bukan Usernyaa!!"
            })
            return
        }
        next()
    }catch (error) {
        console.log(error)
    }
}

module.exports = TroopsApproval