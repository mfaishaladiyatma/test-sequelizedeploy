const authentication = require("./authentication")
const TroopsApproval = require("./TroopsApproval")

module.exports = {
    authentication,
    TroopsApproval
}